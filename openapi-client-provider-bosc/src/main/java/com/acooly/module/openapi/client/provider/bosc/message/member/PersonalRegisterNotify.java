/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-22 13:28 创建
 */
package com.acooly.module.openapi.client.provider.bosc.message.member;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscNotify;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.member.BoscAccessTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.member.BoscAuditStatusEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscBankcodeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.member.BoscIdCardTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.member.BoscUserRoleEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhangpu 2017-09-22 13:28
 */
@ApiMsgInfo(service = BoscServiceNameEnum.PERSONAL_REGISTER_EXPAND, type = ApiMessageType.Notify)
public class PersonalRegisterNotify extends BoscNotify {

    @NotEmpty
    @Size(max = 50)
    private String platformUserNo;

    @NotEmpty
    @Size(max = 50)
    private String requestNo;

    @NotEmpty
    @Size(max = 50)
    private String realName;

    @NotEmpty
    @Size(max = 50)
    private String idCardNo;

    @NotEmpty
    @Size(max = 50)
    private String mobile;

    @NotEmpty
    private BoscIdCardTypeEnum idCardType;
    @NotEmpty
    private BoscUserRoleEnum userRole;

    @NotEmpty
    private String bankcardNo;

    @NotEmpty
    private BoscBankcodeEnum bankcode;

    private BoscAccessTypeEnum accessType;

    private BoscAuditStatusEnum auditStatus;

    public String getPlatformUserNo() {
        return platformUserNo;
    }

    public void setPlatformUserNo(String platformUserNo) {
        this.platformUserNo = platformUserNo;
    }

    public String getRequestNo() {
        return requestNo;
    }

    public void setRequestNo(String requestNo) {
        this.requestNo = requestNo;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getIdCardNo() {
        return idCardNo;
    }

    public void setIdCardNo(String idCardNo) {
        this.idCardNo = idCardNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public BoscIdCardTypeEnum getIdCardType() {
        return idCardType;
    }

    public void setIdCardType(BoscIdCardTypeEnum idCardType) {
        this.idCardType = idCardType;
    }

    public BoscUserRoleEnum getUserRole() {
        return userRole;
    }

    public void setUserRole(BoscUserRoleEnum userRole) {
        this.userRole = userRole;
    }

    public String getBankcardNo() {
        return bankcardNo;
    }

    public void setBankcardNo(String bankcardNo) {
        this.bankcardNo = bankcardNo;
    }
    
    public BoscBankcodeEnum getBankcode () {
        return bankcode;
    }
    
    public void setBankcode (BoscBankcodeEnum bankcode) {
        this.bankcode = bankcode;
    }
    
    public BoscAccessTypeEnum getAccessType () {
        return accessType;
    }
    
    public void setAccessType (BoscAccessTypeEnum accessType) {
        this.accessType = accessType;
    }
    
    public BoscAuditStatusEnum getAuditStatus () {
        return auditStatus;
    }
    
    public void setAuditStatus (BoscAuditStatusEnum auditStatus) {
        this.auditStatus = auditStatus;
    }
}
