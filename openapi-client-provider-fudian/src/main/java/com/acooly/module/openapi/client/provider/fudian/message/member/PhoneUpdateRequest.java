/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-10 03:05:39 创建
 */package com.acooly.module.openapi.client.provider.fudian.message.member;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianRequest;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-02-10 03:05:40
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.PHONE_UPDATE ,type = ApiMessageType.Request)
public class PhoneUpdateRequest extends FudianRequest {

    /**
     * 账户号
     * 用户在本系统的唯一账户编号，由本系统生成.
     */
    @NotEmpty
    @Length(max=50)
    private String accountNo;

    /**
     * 新手机号
     * 需要修改的新的手机号码
     */
    @NotEmpty
    @Length(min = 11,max=11)
    private String newPhone;

    /**
     * 修改方式
     * 修改手机方式：
     * 1原有手机号可用，进行自助修改
     * 2原有手机号不可用，进行人工修改
     */
    @NotEmpty
    @Length(min = 1,max=1)
    private String type;

    /**
     * 用户名
     * 用户在本系统的唯一标识，由本系统生成
     */
    @NotEmpty
    @Length(max=32)
    private String userName;
}