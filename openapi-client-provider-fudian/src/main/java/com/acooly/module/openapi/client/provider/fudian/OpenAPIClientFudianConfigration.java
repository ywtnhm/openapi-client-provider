package com.acooly.module.openapi.client.provider.fudian;

import com.acooly.integration.web.SpringFilterProxy;
import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.fudian.file.FudianSftpFileHandler;
import com.acooly.module.openapi.client.provider.fudian.notify.FudianApiServiceClientFilter;
import com.esotericsoftware.minlog.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import static com.acooly.module.openapi.client.provider.fudian.OpenAPIClientFudianProperties.PREFIX;


@EnableConfigurationProperties({OpenAPIClientFudianProperties.class})
@ConditionalOnProperty(value = PREFIX + ".enable", matchIfMissing = true)
@ComponentScan
@ComponentScan("com.acooly.module.openapi.client.file")
public class OpenAPIClientFudianConfigration {

    @Autowired
    private OpenAPIClientFudianProperties openAPIClientFudianProperties;

    @Bean("fudianHttpTransport")
    public Transport fuiouHttpTransport() {
        HttpTransport httpTransport = new HttpTransport();
        httpTransport.setGateway(openAPIClientFudianProperties.getGatewayUrl());
        httpTransport.setConnTimeout(String.valueOf(openAPIClientFudianProperties.getConnTimeout()));
        httpTransport.setReadTimeout(String.valueOf(openAPIClientFudianProperties.getReadTimeout()));
        return httpTransport;
    }


    @Bean
    public FilterRegistrationBean fudianApiServiceClientFilterRegistrationBean(FudianApiServiceClientFilter fudianApiServiceClientFilter) {
        FilterRegistrationBean bean = new FilterRegistrationBean(fudianApiServiceClientFilter);
        bean.setEnabled(false);
        return bean;
    }

    /**
     * 接受异步通知的filter
     *
     * @return
     */
    @Bean
    public FilterRegistrationBean fudianClientFilter() {
        FilterRegistrationBean bean = new FilterRegistrationBean();
        bean.setFilter(new SpringFilterProxy());
        bean.addInitParameter("targetBeanName", "fudianApiServiceClientFilter");
        String notifyUrlPattern = FudianConstants.getCanonicalUrl(openAPIClientFudianProperties.getNotifyUrl(), "/*");
        Log.info("fudianClientFilter notify url pattern: {}", notifyUrlPattern);
        bean.addUrlPatterns(notifyUrlPattern);
        return bean;
    }


    @Bean("fudianSftpFileHandler")
    public FudianSftpFileHandler fudianSftpFileHandler() {
        FudianSftpFileHandler fudianSftpFileHandler = new FudianSftpFileHandler();
        fudianSftpFileHandler.setHost(openAPIClientFudianProperties.getCheckfile().getHost());
        fudianSftpFileHandler.setPort(openAPIClientFudianProperties.getCheckfile().getPort());
        fudianSftpFileHandler.setUsername(openAPIClientFudianProperties.getCheckfile().getUsername());
        fudianSftpFileHandler.setPassword(openAPIClientFudianProperties.getCheckfile().getPassword());
        fudianSftpFileHandler.setServerRoot(openAPIClientFudianProperties.getCheckfile().getServerRoot());
        fudianSftpFileHandler.setLocalRoot(openAPIClientFudianProperties.getCheckfile().getLocalRoot());
        return fudianSftpFileHandler;
    }


}
