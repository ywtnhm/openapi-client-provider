/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-22 04:32:16 创建
 */
package com.acooly.module.openapi.client.provider.fudian.message.query;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianResponse;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import com.acooly.module.openapi.client.provider.fudian.message.query.dto.AccountLog;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

/**
 * @author zhangpu 2018-02-22 04:32:16
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.QUERY_LOGACCOUNT, type = ApiMessageType.Response)
public class QueryLogAccountResponse extends FudianResponse {

    /**
     * 资金流水列表
     * json格式："accountLogList：[{“”:””}
     */
    @NotEmpty
    @Length(max = 5000)
    private List<AccountLog> accountLogList;

    /**
     * 账户号
     * 用户在本系统的唯一账户编号，由本系统生成.
     */
    @NotEmpty
    @Length(max = 50)
    private String accountNo;

    /**
     * 商户号
     * 用于校验主体参数和业务参数一致性，保证参数的安全传输
     */
    @NotEmpty
    @Length(max = 8)
    private String merchantNo;

    /**
     * 查询订单日期
     * 查询日期
     */
    @NotEmpty
    @Length(min = 8, max = 8)
    private String queryOrderDate;

    /**
     * 用户名
     * 用户在本系统的唯一标识，由本系统生成
     */
    @NotEmpty
    @Length(max = 32)
    private String userName;
}