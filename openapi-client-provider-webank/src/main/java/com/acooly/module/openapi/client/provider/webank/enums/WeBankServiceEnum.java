/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-24 19:29 创建
 */
package com.acooly.module.openapi.client.provider.webank.enums;

import com.google.common.collect.Maps;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * BOSC服务名称枚举
 *
 * @author zhangpu 2017-09-24 19:29
 */
public enum WeBankServiceEnum implements Messageable {

    WEBANK_SMS("11", "微众短息发送"),
    WEBANK_SIGN("09", "微众签约"),
    WEBANK_DEDUCT("02", "微众代扣"),
    WEBANK_WITHDRAW("01", "微众代付"),
    WEBANK_DEDUCT_QUERY("1002", "微众代扣查询"),
    WEBANK_WITHDRAW_QUERY("1001", "微众代付查询"),
    ;

    private final String code;
    private final String message;

    private WeBankServiceEnum (String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = Maps.newLinkedHashMap();
        for (WeBankServiceEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static WeBankServiceEnum find(String code) {
        for (WeBankServiceEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        throw new IllegalArgumentException("BoscServiceNameEnum not legal:" + code);
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<WeBankServiceEnum> getAll() {
        List<WeBankServiceEnum> list = new ArrayList<WeBankServiceEnum>();
        for (WeBankServiceEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (WeBankServiceEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }


}
