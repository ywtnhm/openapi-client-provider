package com.acooly.module.openapi.client.provider.alipay;

/**
 * @author zhangpu 2017-09-17 17:49
 */
public class AlipayConstants {
    /**
     * 签名实现KEY和秘钥
     */
    public static final String PROVIDER_NAME = "alipayProvider";
    public static final String ORDER_NO_NAME = "out_trade_no";

    public static final String SIGN = "sign";
    public static final String SIGNER_TYPE = "alipayMd5";
    
    public static String SUCCESS_CODE = "10000";
    
    public static String SUCCESS_TRADE_STATUS = "TRADE_SUCCESS";
    
    public static String PROCESSING_TRADE_STATUS = "WAIT_BUYER_PAY";
    /**
     * 返回格式
     */
    public static String FORMAT = "json";
	/**
     * 编码
     */
    public static String CHARSET = "UTF-8";
    /**
     * 签名类型
     */
    public static String SIGN_TYPE = "RSA2";
}
