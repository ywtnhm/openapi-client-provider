package com.acooly.module.openapi.client.provider.yl.message.dto;


import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class YlBillDownLoadInfo implements Serializable {

    /**
     * 入账的时间 yyyy-MM-dd
     */
    private String account_date;
}
