/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-25 17:56 创建
 */
package com.acooly.openapi.client.test.fudian;

import com.acooly.module.openapi.client.provider.fudian.FudianApiService;
import com.acooly.module.openapi.client.provider.fudian.OpenAPIClientFudianProperties;
import com.acooly.module.openapi.client.provider.fudian.message.member.UserRegisterRequest;
import com.acooly.openapi.client.test.fudian.support.IdCardGenerator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 上海银行P2P存管 控制层Demo和test
 *
 * @author zhangpu 2017-09-25 17:56
 */
@Slf4j
@Controller
@RequestMapping("/openapi/client/fudian")
public class FudianClientController {

    @Autowired
    private FudianApiService fudianApiService;

    @Autowired
    private OpenAPIClientFudianProperties openAPIClientFudianProperties;


    /**
     * 注册: 跳转银行
     *
     * @param request
     * @param model
     * @return
     */
    @RequestMapping("userRegister")
    public Object userRegister(HttpServletRequest request, Model model) {
        String realName = request.getParameter("realName");
        String mobileNo = request.getParameter("mobilePhone");
        String identityCode = IdCardGenerator.generate();
        String domain = openAPIClientFudianProperties.getDomain();
        String returnUrl = domain + "/openapi/client/fudian/userRegisterReturn.html";
        //String notifyUrl = domain + "/openapi/client/fudian/notify/user/register";
        UserRegisterRequest req = new UserRegisterRequest();
        req.setRealName(realName);
        req.setIdentityCode(identityCode);
        req.setMobilePhone(mobileNo);
        req.setReturnUrl(returnUrl);
        //req.setNotifyUrl(notifyUrl);
        String formHtml = fudianApiService.userRegister(req);
        model.addAttribute("formHtml", formHtml);
        log.info(formHtml);
        return "/fudian/userRegister";
    }


    /**
     * 注册：银行return
     * (测试后，貌似没有异步通知，只有同步)
     * <p>
     * {"service":"/user/register","partner":null,"retCode":"0000","retMsg":"操作成功","orderDate":"20180131","orderNo":"18013116150674530001",
     * "accountNo":"UA02551119723061001","extMark":"","identityType":"1","identityCode":"51022119820915641X","returnUrl":"http://prosysoft.cn:9090/openapi/client/fudian/userRegisterReturn.html","notifyUrl":"http://prosysoft.cn:9090/openapi/client/fudian/userRegisterReturn.html","merchantNo":"M02538118117201001","mobilePhone":"13896177630","regDate":"20180131","realName":"张浦",
     * "userName":"UU02551119718871001"}
     *
     * @param request
     * @return
     */
    @RequestMapping("userRegisterReturn")
    @ResponseBody
    public Object userRegister(HttpServletRequest request) {
        try {
            String data = IOUtils.toString(request.getInputStream(), "utf-8");
            //Map<String, String> data = Servlets.getParameters(request, null, false);
            log.info("Fudian同步通知：{}", data);
            return data;
            //UserRegisterNotify notify = fudianApiService.userRegisterNotice(data);
            //return notify;
        } catch (Exception e) {
            log.warn("Fudian同步通知 处理失败.", e);
        }
        return null;
    }
//
//
//	@RequestMapping("commonReturn")
//	@ResponseBody
//	public Object commonReturn (HttpServletRequest request) {
//		return fudianApiService.commonNotifyParser (Servlets.getParameters (request, null, false));
//	}
//
//	@RequestMapping("withdraw")
//	public String withdraw (Model model) {
//		WithdrawRequest request = new WithdrawRequest (investorPlatformUserNo, Ids.oid (),
//		                                               Dates.addDate (new Date (), 3 * 60 * 1000), redirectUrl,
//		                                               new Money (5000),
//		                                               null);
//		model.addAttribute ("formHtml", fundApiService.withdrawRedirect (request));
//		return redirectJumpPager;
//	}
//
//
//	@RequestMapping("recharge")
//	public String recharge (Model model, FudianRechargeWayEnum rechargeWayEnum) {
//		RechargeRequest request = new RechargeRequest (investorPlatformUserNo, Ids.oid (), new Money (5000), null,
//		                                               rechargeWayEnum, redirectUrl,
//		                                               Dates.addDate (new Date (), 3 * 60 * 1000));
//		request.setExpectPayCompany (FudianExpectPayCompanyEnum.YEEPAY);
//		request.setBankcode (FudianBankcodeEnum.ABOC);
//		model.addAttribute ("formHtml", fundApiService.rechargeRedirect (request));
//
//		return redirectJumpPager;
//	}
//
//	@RequestMapping("unbindBankCard")
//	public String unbindBankCard(Model model){
//		UnbindBankcardRequest request = new UnbindBankcardRequest ("201702050677", Ids.getDid (), redirectUrl);
//		model.addAttribute ("formHtml",memberApiService.unbindBankcard (request));
//		return redirectJumpPager;
//	}
}
