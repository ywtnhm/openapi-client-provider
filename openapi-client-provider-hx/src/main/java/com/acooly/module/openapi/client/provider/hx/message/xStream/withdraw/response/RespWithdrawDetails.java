package com.acooly.module.openapi.client.provider.hx.message.xStream.withdraw.response;


import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Data;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("Details")
public class RespWithdrawDetails {

    @XStreamAlias("Detail")
    private RespWithdrawDetail detail;
}
