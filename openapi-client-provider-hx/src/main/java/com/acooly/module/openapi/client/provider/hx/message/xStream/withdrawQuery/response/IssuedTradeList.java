package com.acooly.module.openapi.client.provider.hx.message.xStream.withdrawQuery.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Data;

/**
 * @author fufeng 2018/3/2 10:03.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("IssuedTradeList")
public class IssuedTradeList {

    @XStreamAlias("issuedTrade")
    private IssuedTrade issuedTrade;

}
