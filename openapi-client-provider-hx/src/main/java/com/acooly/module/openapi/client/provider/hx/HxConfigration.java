package com.acooly.module.openapi.client.provider.hx;

import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import static com.acooly.module.openapi.client.provider.hx.HxProperties.PREFIX;


@EnableConfigurationProperties({HxProperties.class})
@ConditionalOnProperty(value = PREFIX + ".enable", matchIfMissing = true)
@ComponentScan
public class HxConfigration {

    @Autowired
    private HxProperties hxProperties;

    @Bean("hxHttpTransport")
    public Transport cmbHttpTransport() {
        HttpTransport httpTransport = new HttpTransport();
        httpTransport.setGateway(hxProperties.getGatewayUrl());
        httpTransport.setConnTimeout(String.valueOf(hxProperties.getConnTimeout()));
        httpTransport.setReadTimeout(String.valueOf(hxProperties.getReadTimeout()));
        return httpTransport;
    }
}
