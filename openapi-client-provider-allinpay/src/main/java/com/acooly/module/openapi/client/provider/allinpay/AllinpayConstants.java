/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-17 17:49 创建
 */
package com.acooly.module.openapi.client.provider.allinpay;

import com.acooly.core.utils.Strings;

/**
 * @author zhike 2017-09-17 17:49
 */
public class AllinpayConstants {
    /**
     * 签名实现KEY和秘钥
     */
    public static final String PROVIDER_NAME_CERT = "allinpayProviderCert";
    public static final String PROVIDER_NAME_MD5 = "allinpayProviderMd5";
    public static final String PROVIDER_DEF_PRINCIPAL = "principal";

    public static final String MOTIFY_MESSAGE = "notifyMessage";
    public static final String SIGN_NAME = "sign";
    public static final String POS_PARTNER_KEY = "cusid";
    public static final String MD5_SIGNER_TYPE = "allinpayMd5Hex";
    public static final String FIX_NOTIFY_URL = "/gateway/notify/allinpayNotify/";

    public static final String NETBANK_NAME = "通联支付网关";

    public static String getCanonicalUrl(String gatewayUrl, String serviceName) {
        String serviceUrl = gatewayUrl;
        serviceUrl = Strings.removeEnd(serviceUrl, "/");
        if (!Strings.startsWith(serviceName, "/")) {
            serviceUrl = serviceUrl + "/" + serviceName;
        } else {
            serviceUrl = serviceUrl + serviceName;
        }
        return serviceUrl;
    }
}
