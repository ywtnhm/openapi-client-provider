package com.acooly.module.openapi.client.provider.jyt.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author zhike 2018/5/8 17:49
 */
@Getter
@Setter
@XStreamAlias("body")
public class JytRetrievesSmsCodeResponseBody implements Serializable {

    /**
     * 支付状态
     当报文头响应码为S0000000时有值。
     当响应码为S0000000（其它值时，均属于获取失败）时有值。
     01订单信息验证成功（验证码已发送）
     02重新获取验证码失败
     03订单已过期（无法重新获取短信）
     */
    @NotBlank
    @XStreamAlias("tran_state")
    private String tranState;

    /**
     * 备注信息
     * 结果说明
     */
    @Size(max = 60)
    @XStreamAlias("remark")
    private String remark;
}
