package com.acooly.module.openapi.client.provider.jyt.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.jyt.domain.JytApiMsgInfo;
import com.acooly.module.openapi.client.provider.jyt.domain.JytResponse;
import com.acooly.module.openapi.client.provider.jyt.enums.JytServiceEnum;
import com.acooly.module.openapi.client.provider.jyt.message.dto.JytCheckCardApplyResponseBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/6/14 15:28
 */
@Getter
@Setter
@JytApiMsgInfo(service = JytServiceEnum.CHECK_CARD_APPLY,type = ApiMessageType.Response)
@XStreamAlias("message")
public class JytCheckCardApplyResponse extends JytResponse {

    /**
     * 响应报文体
     */
    @XStreamAlias("body")
    private JytCheckCardApplyResponseBody checkCardApplyBody;
}
