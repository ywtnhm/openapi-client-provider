package com.acooly.module.openapi.client.provider.jyt.message;

import com.acooly.core.utils.validate.Validators;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.jyt.domain.JytApiMsgInfo;
import com.acooly.module.openapi.client.provider.jyt.domain.JytRequest;
import com.acooly.module.openapi.client.provider.jyt.enums.JytServiceEnum;
import com.acooly.module.openapi.client.provider.jyt.message.dto.JytCheckCardApplyRequestBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/6/14 15:28
 */
@Getter
@Setter
@JytApiMsgInfo(service = JytServiceEnum.CHECK_CARD_APPLY,type = ApiMessageType.Request)
@XStreamAlias("message")
public class JytCheckCardApplyRequest extends JytRequest {

    /**
     * 请求报文体
     */
    @XStreamAlias("body")
    private JytCheckCardApplyRequestBody checkCardApplyRequestBody;

    @Override
    public void doCheck() {
        Validators.assertJSR303(getHeaderRequest());
        Validators.assertJSR303(getCheckCardApplyRequestBody());
    }
}
