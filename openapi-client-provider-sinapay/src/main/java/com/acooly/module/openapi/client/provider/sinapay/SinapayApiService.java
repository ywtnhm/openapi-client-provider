/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-25 14:40 创建
 */
package com.acooly.module.openapi.client.provider.sinapay;

import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayNotify;
import com.acooly.module.openapi.client.provider.sinapay.message.member.*;
import com.acooly.module.openapi.client.provider.sinapay.message.trade.*;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author zhike 2017-09-25 14:40
 */
@Component
public class SinapayApiService {

    @Resource(name = "sinapayApiServiceClient")
    private SinapayApiServiceClient sinapayApiServiceClient;


    /**
     * 2.1	创建激活会员
     * @param request
     * @return
     */
    public CreateActivateMemberResponse createActivateMember(CreateActivateMemberRequest request) {
        return (CreateActivateMemberResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.2	设置实名信息
     * @param request
     * @return
     */
    public SetRealNameResponse setRealName(SetRealNameRequest request) {
        return (SetRealNameResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.3	设置支付密码重定向
     * @param request
     * @return
     */
    public SetPayPasswordResponse setPayPassword(SetPayPasswordRequest request) {
        return (SetPayPasswordResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.4	修改支付密码重定向
     * @param request
     * @return
     */
    public ModifyPayPasswordResponse modifyPayPassword(ModifyPayPasswordRequest request) {
        return (ModifyPayPasswordResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.5	找回支付密码重定向
     * @param request
     * @return
     */
    public FindPayPasswordResponse findPayPassword(FindPayPasswordRequest request) {
        return (FindPayPasswordResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.6	查询是否设置支付密码
     * @param request
     * @return
     */
    public QuerySetPayPasswordResponse querySetPayPassword(QuerySetPayPasswordRequest request) {
        return (QuerySetPayPasswordResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.7	绑定银行卡
     * @param request
     * @return
     */
    public BindingBankCardResponse bindingBankCard(BindingBankCardRequest request) {
        return (BindingBankCardResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.8	绑定银行卡推进
     * @param request
     * @return
     */
    public BindingBankCardAdvanceResponse bindingBankCardAdvance(BindingBankCardAdvanceRequest request) {
        return (BindingBankCardAdvanceResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.9	解绑银行卡
     * @param request
     * @return
     */
    public UnbindingBankCardResponse unBindingBankCard(UnbindingBankCardRequest request) {
        return (UnbindingBankCardResponse)sinapayApiServiceClient.execute(request);
    }


    /**
     * 2.10	解绑银行卡推进
     * @param request
     * @return
     */
    public UnbindingBankCardAdvanceResponse unBindingBankCardAdvance(UnbindingBankCardAdvanceRequest request) {
        return (UnbindingBankCardAdvanceResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.11	查询银行卡
     * @param request
     * @return
     */
    public QueryBankCardResponse  queryBankCard(QueryBankCardRequest request) {
        return (QueryBankCardResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.12	查询余额/基金份额
     * @param request
     * @return
     */
    public QueryBalanceResponse  queryBalance(QueryBalanceRequest request) {
        return (QueryBalanceResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.13	查询收支明细
     * @param request
     * @return
     */
    public QueryAccountDetailsResponse  queryAccountDetails(QueryAccountDetailsRequest request) {
        return (QueryAccountDetailsResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.14	冻结余额
     * @param request
     * @return
     */
    public BalanceFreezeResponse  balanceFreeze(BalanceFreezeRequest request) {
        return (BalanceFreezeResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.15	解冻余额
     * @param request
     * @return
     */
    public BalanceUnfreezeResponse  balanceUnfreeze(BalanceUnfreezeRequest request) {
        return (BalanceUnfreezeResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.16	查询冻结解冻结果
     * @param request
     * @return
     */
    public QueryCtrlResultResponse queryCtrlResult(QueryCtrlResultRequest request) {
        return (QueryCtrlResultResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.17	查询企业会员信息
     * @param request
     * @return
     */
    public QueryMemberInfosResponse queryMemberInfos(QueryMemberInfosRequest request) {
        return (QueryMemberInfosResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.18	查询企业会员审核结果
     * @param request
     * @return
     */
    public QueryAuditResultResponse queryAuditResult(QueryAuditResultRequest request) {
        return (QueryAuditResultResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.19	请求审核企业会员资质
     * @param request
     * @return
     */
    public AuditMemberInfosResponse auditMemberInfos(AuditMemberInfosRequest request) {
        return (AuditMemberInfosResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.20	经办人信息
     * @param request
     * @return
     */
    public SmtFundAgentBuyResponse smtFundAgentBuy(SmtFundAgentBuyRequest request) {
        return (SmtFundAgentBuyResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.21	查询经办人信息
     * @param request
     * @return
     */
    public QueryFundAgentBuyResponse queryFundAgentBuy(QueryFundAgentBuyRequest request) {
        return (QueryFundAgentBuyResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.22	sina页面展示用户信息
     * @param request
     * @return
     */
    public ShowMemberInfosSinaResponse showMemberInfosSina(ShowMemberInfosSinaRequest request) {
        return (ShowMemberInfosSinaResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.23	修改认证手机
     * @param request
     * @return
     */
    public ModifyVerifyMobileResponse modifyVerifyMobile(ModifyVerifyMobileRequest request) {
        return (ModifyVerifyMobileResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.24	找回认证手机
     * @param request
     * @return
     */
    public FindVerifyMobileResponse findVerifyMobile(FindVerifyMobileRequest request) {
        return (FindVerifyMobileResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.25	修改银行预留手机
     * @param request
     * @return
     */
    public ChangeBankMobileResponse  changeBankMobile(ChangeBankMobileRequest request) {
        return (ChangeBankMobileResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.26	修改银行预留手机推进
     * @param request
     * @return
     */
    public ChangeBankMobileAdvanceResponse  changeBankMobileAdvance(ChangeBankMobileAdvanceRequest request) {
        return (ChangeBankMobileAdvanceResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.27	查询交易关联号下可代付金额
     * @param request
     * @return
     */
    public QueryTradeRelatedNoResponse  queryTradeRelatedNo(QueryTradeRelatedNoRequest request) {
        return (QueryTradeRelatedNoResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 2.28	查询用户基金剩余额度
     * @param request
     * @return
     */
    public QueryFundQuotaResponse  queryFundQuota(QueryFundQuotaRequest request) {
        return (QueryFundQuotaResponse)sinapayApiServiceClient.execute(request);
    }
    /**
     * 同步，异步通知转化
     * serviceKey :SinapayServiceNameEnum枚举中取对应的code
     * @param request 请求
     * @param serviceKey 服务标识
     * @return
     */
    public SinapayNotify notice(HttpServletRequest request, String serviceKey) {
        return  sinapayApiServiceClient.notice(request, serviceKey);
    }

    /**
     * 3.1 创建代收交易
     * @param request
     * @return
     */
    public CreateHostingCollectTradeResponse createHostingCollectTrade(CreateHostingCollectTradeRequest request){
        return (CreateHostingCollectTradeResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 3.2 创建代付交易
     * @param request
     * @return
     */
    public CreateSingleHostingPayTradeResponse createSingleHostingPayTrade(CreateSingleHostingPayTradeRequest request){
        return (CreateSingleHostingPayTradeResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 3.3 创建批量代付交易
     * @param request
     * @return
     */
    public CreateBatchHostingPayTradeResponse createBatchHostingPayTrade(CreateBatchHostingPayTradeRequest request){
        return (CreateBatchHostingPayTradeResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 3.4 交易支付
     * @param request
     * @return
     */
    public PayHostingTradeResponse payHostingTrade(PayHostingTradeRequest request){
        return (PayHostingTradeResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 3.5 支付结果查询
     * @param request
     * @return
     */
    public QueryPayResultResponse queryPayResult(QueryPayResultRequest request){
        return (QueryPayResultResponse) sinapayApiServiceClient.execute(request);
    }

    /**
     * 3.6 交易查询
     * @param request
     * @return
     */
    public QueryHostingTradeResponse queryHostingTrade(QueryHostingTradeRequest request){
        return (QueryHostingTradeResponse) sinapayApiServiceClient.execute(request);
    }

    /**
     * 3.7  交易批次查询
     * @param request
     * @return
     */
    public QueryHostingBatchTradeResponse queryHostingBatchTrade(QueryHostingBatchTradeRequest request){
        return (QueryHostingBatchTradeResponse) sinapayApiServiceClient.execute(request);
    }

    /**
     * 3.8 退款
     * 针对代收交易，可发起退款（存钱罐暂不支持部分退款）。退款的前提是原交易已经处理完毕。
     * @param request
     * @return
     */
    public CreateHostingRefundResponse createHostingRefund(CreateHostingRefundRequest request){
        return (CreateHostingRefundResponse) sinapayApiServiceClient.execute(request);
    }

    /**
     * 3.9  退款查询
     * @param request
     * @return
     */
    public QueryHostingRefundResponse queryHostingRefund(QueryHostingRefundRequest request){
        return (QueryHostingRefundResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 3.10 充值
     * @param request
     * @return
     */
    public CreateHostingDepositResponse createHostingDeposit(CreateHostingDepositRequest request){
        return (CreateHostingDepositResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 3.11 充值查询
     * @param request
     * @return
     */
    public QueryHostingDepositResponse queryHostingDeposit(QueryHostingDepositRequest request){
        return (QueryHostingDepositResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 3.12 提现
     * @param request
     * @return
     */
    public CreateHostingWithdrawResponse createHostingWithdraw(CreateHostingWithdrawRequest request){
        return (CreateHostingWithdrawResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 3.13 提现查询
     * @param request
     * @return
     */
    public QueryHostingWithdrawResponse queryHostingWithdraw(QueryHostingWithdrawRequest request){
        return (QueryHostingWithdrawResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 3.14 创建单笔代付到提现卡交易
     * @param request
     * @return
     */
    public CreateSingleHostingPayToCardTradeResponse createSingleHostingPayToCardTrade(CreateSingleHostingPayToCardTradeRequest request){
        return (CreateSingleHostingPayToCardTradeResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 3.15 创建批量代付到提现卡交易
     */
    public CreateBatchHostingPayToCardTradeResponse createBatchHostingPayToCardTrade(CreateBatchHostingPayToCardTradeRequest request){
        return (CreateBatchHostingPayToCardTradeResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 3.16	代收完成
     * @param request
     * @return
     */
    public FinishPreAuthTradeResponse finishPreAuthTrade(FinishPreAuthTradeRequest request) {
        return (FinishPreAuthTradeResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 3.17	代收撤销
     * @param request
     * @return
     */
    public CancelPreAuthTradeResponse cancelPreAuthTrade(CancelPreAuthTradeRequest request) {
        return (CancelPreAuthTradeResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 委托扣款重定向
     * @param request
     * @return
     */
    public HandleWithholdAuthorityResponse handleWithholdAuthority(HandleWithholdAuthorityRequest request) {
        return (HandleWithholdAuthorityResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 查看用户是否委托扣款
     * @param request
     * @return
     */
    public QueryWithholdAuthorityResponse queryWithholdAuthority(QueryWithholdAuthorityRequest request) {
        return (QueryWithholdAuthorityResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 查询认证信息
     * @param request
     * @return
     */
    public QueryVerifyResponse queryVerify(QueryVerifyRequest request) {
        return (QueryVerifyResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 绑定认证信息
     * @param request
     * @return
     */
    public BindingVerifyResponse bindingVerify(BindingVerifyRequest request) {
        return (BindingVerifyResponse)sinapayApiServiceClient.execute(request);
    }

    /**
     * 解绑认证信息
     * @param request
     * @return
     */
    public UnbindingVerifyResponse unbindingVerify(UnbindingVerifyRequest request) {
        return (UnbindingVerifyResponse)sinapayApiServiceClient.execute(request);
    }
}
