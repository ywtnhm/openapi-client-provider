package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.CANCEL_PRE_AUTH_TRADE, type = ApiMessageType.Response)
public class CancelPreAuthTradeResponse extends SinapayResponse{
	
}

    