/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月1日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.domain;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhike
 */
public class SinapayRedirect extends SinapayResponse {
	@NotEmpty
	@ApiItem("redirect_url")
	private String redirectUrl;

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
}
