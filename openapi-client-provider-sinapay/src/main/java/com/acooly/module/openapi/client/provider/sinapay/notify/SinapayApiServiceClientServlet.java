/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2018-01-31 16:37 创建
 */
package com.acooly.module.openapi.client.provider.sinapay.notify;


import com.acooly.core.common.exception.BusinessException;
import com.acooly.core.common.web.servlet.AbstractSpringServlet;
import com.acooly.core.utils.Servlets;
import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.notify.NotifyHandlerDispatcher;
import com.acooly.module.openapi.client.provider.sinapay.OpenAPIClientSinapayProperties;
import com.acooly.module.openapi.client.provider.sinapay.SinapayConstants;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zhike 2018-01-31 16:37
 */
@Slf4j
public class SinapayApiServiceClientServlet extends AbstractSpringServlet {

    private NotifyHandlerDispatcher notifyHandlerDispatcher;

    private static final int SUCCESS_RESPONSE_CODE = 200;
    private static final String SUCCESS_RESPONSE_BODY = "SUCCESS";
    private static final String FAILED_RESPONSE_BODY = "SUCCESS";
    private static final String DEFAULT_NOTIFY_DISPATCHER_BEAN_NAME = "clientNotifyHandlerDispatcher";

    public static final String SUCCESS_RESPONSE_CODE_KEY = "success_response_code";
    public static final String SUCCESS_RESPONSE_BODY_KEY = "success_response_body";
    public static final String NOTIFY_DISPATCHER_BEAN_NAME_KEY = "NOTIFY_DISPATCHER_BEAN_NAME_KEY";
    @Autowired
    private OpenAPIClientSinapayProperties openAPIClientSinapayProperties;

    @Override
    public void doService(HttpServletRequest request, HttpServletResponse response) {
        try {
            Map<String, Object> received = Servlets.getParametersStartingWith(request, null);
            Map<String, String> notifyData = new HashMap<String, String>(received.size());
            for (Map.Entry<String, Object> entry : received.entrySet()) {
                notifyData.put(entry.getKey(), (String) entry.getValue());
            }
            String notifyUrl = Servlets.getRequestPath(request);
            notifyHandlerDispatcher.dispatch(notifyUrl, notifyData);
            response.setStatus(getSuccessResponseCode());
            Servlets.writeResponse(response, getSuccessResponseBody(), null);
        } catch (Exception e) {
            Servlets.writeResponse(response,  "failure", null);
        }
    }

    @Override
    public void doInit() {
        super.doInit();
        notifyHandlerDispatcher = getBean(getDispatcherBeanName(), NotifyHandlerDispatcher.class);
    }

    @Override
    public void destroy() {
        log.info("Sinapay异步通知接受Filter销毁");
    }

    protected String getDispatcherBeanName() {
        String beanName = getInitParameter(NOTIFY_DISPATCHER_BEAN_NAME_KEY);
        if (Strings.isBlank(beanName)) {
            beanName = DEFAULT_NOTIFY_DISPATCHER_BEAN_NAME;
        }
        return beanName;
    }

    protected String getSuccessResponseBody(){
        String body = getInitParameter(SUCCESS_RESPONSE_BODY_KEY);
        if(Strings.isBlank(body)){
            body = SUCCESS_RESPONSE_BODY;
        }
        return body;
    }

    protected int getSuccessResponseCode(){
        try {
            return Integer.parseInt(getInitParameter(SUCCESS_RESPONSE_CODE_KEY));
        } catch (Exception e) {
            return SUCCESS_RESPONSE_CODE;
        }
    }
}
