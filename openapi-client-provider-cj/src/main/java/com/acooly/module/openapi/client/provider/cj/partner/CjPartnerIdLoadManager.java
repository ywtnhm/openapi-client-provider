/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-11-15 13:21 创建
 */
package com.acooly.module.openapi.client.provider.cj.partner;

import com.acooly.module.openapi.client.provider.cj.domain.CjResponse;

/**
 * 标记KeyStoreInfo的load 标记接口
 *
 * @author zhangpu 2017-11-15 13:21
 */
public interface CjPartnerIdLoadManager {

    /**
     * 获取当前订单对应的商户id
     *
     * @param cjResponse
     * @return
     */
    String load(CjResponse cjResponse);
}
