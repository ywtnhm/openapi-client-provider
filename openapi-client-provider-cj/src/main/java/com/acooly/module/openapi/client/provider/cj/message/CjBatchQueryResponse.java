package com.acooly.module.openapi.client.provider.cj.message;


import com.acooly.core.utils.Money;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.cj.domain.CjApiMsgInfo;
import com.acooly.module.openapi.client.provider.cj.domain.CjResponse;
import com.acooly.module.openapi.client.provider.cj.enums.CjServiceEnum;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@CjApiMsgInfo(service = CjServiceEnum.CJ_BATCH_DEDUCT_QUERY, type = ApiMessageType.Response)
public class CjBatchQueryResponse extends CjResponse {

    /**
     * 成功金额
     */
    private Money amountLn;

    /**
     * 清算时间
     */
    private String settleDate;

    /**
     * http返回body
     */
    private String httpBody;

}
