/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-26 02:01 创建
 */
package com.acooly.module.openapi.client.provider.cj.domain;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.cj.enums.CjServiceEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author zhangpu 2017-09-26 02:01
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface CjApiMsgInfo {
    /**
     * 报文类型
     *
     * @return
     */
    ApiMessageType type() default ApiMessageType.Request;

    /**
     * 服务名称
     *
     * @return
     */
    CjServiceEnum service() default CjServiceEnum.CJ_REAL_DEDUCT;

}
