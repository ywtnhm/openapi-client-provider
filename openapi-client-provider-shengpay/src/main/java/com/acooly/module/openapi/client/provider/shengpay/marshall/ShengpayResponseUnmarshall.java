/**
 * create by zhike
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.shengpay.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author zhike
 * @date 2018-1-23
 */
@Slf4j
@Component
public class ShengpayResponseUnmarshall extends ShengpayAbstractMarshall implements ApiUnmarshal<ShengpayResponse, Map<String,String>> {


    @SuppressWarnings("unchecked")
    @Override
    public ShengpayResponse unmarshal(Map<String,String> message, String serviceName) {
        return doUnMarshall(message, serviceName, false);
    }


}
