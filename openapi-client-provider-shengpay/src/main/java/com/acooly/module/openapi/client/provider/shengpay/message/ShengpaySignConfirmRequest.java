package com.acooly.module.openapi.client.provider.shengpay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayApiMsg;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayRequest;
import com.acooly.module.openapi.client.provider.shengpay.enums.ShengpayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/5/17 18:56
 */
@Getter
@Setter
@ShengpayApiMsg(service = ShengpayServiceNameEnum.SIGN_CONFIRM,type = ApiMessageType.Request)
public class ShengpaySignConfirmRequest extends ShengpayRequest {

    /**
     * 签约请求号，必须唯一（商户系统生成的唯一签约请求号）
     */
    @NotBlank
    @Size(max = 32)
    private String requestNo;

    /**
     * 短信验证码
     */
    @NotBlank
    @Size(max = 8)
    private String validateCode;


}
