package com.acooly.module.openapi.client.provider.bosc.message.request;

import org.hibernate.validator.constraints.NotBlank;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequestDomain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PayRequest extends BoscRequestDomain {

	@NotBlank
	private String eAcctNo;

	@MoneyConstraint(min = 1L)
	private Money amount;

}
