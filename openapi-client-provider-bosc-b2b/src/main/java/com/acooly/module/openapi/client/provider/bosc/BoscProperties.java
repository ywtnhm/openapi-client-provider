package com.acooly.module.openapi.client.provider.bosc;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

import static com.acooly.module.openapi.client.provider.bosc.BoscProperties.PREFIX;

@ConfigurationProperties(prefix = PREFIX)
@Getter
@Setter
public class BoscProperties {

	public static final String PREFIX = "acooly.openapi.client.bosc.b2b";

	private String requestUrl;

	private String connTimeout;

	private String readTimeout;

	private String base64key;

	private String certpfx;

	private String certpfxPwd;

	private String key;

}
