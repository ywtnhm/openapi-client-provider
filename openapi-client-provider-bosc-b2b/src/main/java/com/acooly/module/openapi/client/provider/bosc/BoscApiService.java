package com.acooly.module.openapi.client.provider.bosc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponseDomain;
import com.acooly.module.openapi.client.provider.bosc.enums.ActivateStatusEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.OpenTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.message.request.ActivateAcctRequest;
import com.acooly.module.openapi.client.provider.bosc.message.request.ActivateUpBindRequest;
import com.acooly.module.openapi.client.provider.bosc.message.request.DynamicMobileCodeRequest;
import com.acooly.module.openapi.client.provider.bosc.message.request.OpenAcctRequest;
import com.acooly.module.openapi.client.provider.bosc.message.request.PayRequest;
import com.acooly.module.openapi.client.provider.bosc.message.request.QueryAcctStatusRequest;
import com.acooly.module.openapi.client.provider.bosc.message.request.QueryVirAcctBalanceRequest;
import com.acooly.module.openapi.client.provider.bosc.message.request.SendMobileCodeRequest;
import com.acooly.module.openapi.client.provider.bosc.message.request.UpBindCardRequest;
import com.acooly.module.openapi.client.provider.bosc.message.request.VirAcctInRequest;
import com.acooly.module.openapi.client.provider.bosc.message.request.VirAcctWithdrawRequest;
import com.acooly.module.openapi.client.provider.bosc.message.response.ActivateAcctResponse;
import com.acooly.module.openapi.client.provider.bosc.message.response.OpenAcctResponse;
import com.acooly.module.openapi.client.provider.bosc.message.response.QueryAcctStatusResponse;
import com.acooly.module.openapi.client.provider.bosc.message.response.QueryVirAcctBalanceResponse;
import com.acooly.module.openapi.client.provider.bosc.message.response.UpBindCardResponse;
import com.acooly.module.openapi.client.provider.bosc.message.response.VirAcctInResponse;

@Service
public class BoscApiService {

	@Autowired
	private BoscApiServiceClient client;

	/**
	 * 开通资金账户
	 * 
	 * @param request
	 * @return
	 */
	public OpenAcctResponse openAcct(OpenAcctRequest request) {
		request.setChannelUrl("C19SCrAccount.API");
		// request.setChannels("BXKJ");
		request.setService(BoscServiceEnum.openAcct.getKey());

		if (OpenTypeEnum.COMPANY == request.getOpenType()) {
			if (null == request.getGBName() || "".equals(request.getGBName())) {
				request.setGBName("No English Name");
			}
			if (null == request.getCheckerName() || "".equals(request.getCheckerName())) {
				request.setCheckerName(request.getCorpName());
			}
			if (null == request.getCheckerMobile() || "".equals(request.getCheckerMobile())) {
				request.setCheckerMobile(request.getLegalPhone());
			}
			if (null == request.getCheckerIdCard() || "".equals(request.getCheckerIdCard())) {
				request.setCheckerIdCard(request.getLegPerId());
			}
			if (null == request.getDocuOpName() || "".equals(request.getDocuOpName())) {
				request.setDocuOpName(request.getCorpName());
			}
			if (null == request.getDocuOpMobile() || "".equals(request.getDocuOpMobile())) {
				request.setDocuOpMobile(request.getLegalPhone());
			}
			if (null == request.getDocuOpIdCard() || "".equals(request.getDocuOpIdCard())) {
				request.setDocuOpIdCard(request.getLegPerId());
			}
		}

		return (OpenAcctResponse) client.execute(request);
	}

	/**
	 * 申请手机动态验证码（开户）
	 * 
	 * @param request
	 * @return
	 */
	public BoscResponseDomain sendMobileCode(SendMobileCodeRequest request) {
		request.setChannelUrl("C19SendMobileCodeInq.API");
		return client.execute(request);
	}

	public BoscResponseDomain pay(PayRequest request) {
		request.setChannelUrl("TestDaKuan.API");
		return client.execute(request);
	}

	/**
	 * 激活虚账户（企业虚账户需激活）
	 * 
	 * @param request
	 * @return
	 */
	public ActivateAcctResponse activateAcct(ActivateAcctRequest request) {
		request.setChannelUrl("C19ActivateAccInq.API");
		request.setService(BoscServiceEnum.activateAcct.getKey());
		return (ActivateAcctResponse) client.execute(request);
	}

	/**
	 * 查询对公电子户状态交易
	 * 
	 * @param request
	 * @return
	 */
	public QueryAcctStatusResponse queryAcctStatus(QueryAcctStatusRequest request) {
		request.setChannelUrl("C19AccStInq.API");
		request.setService(BoscServiceEnum.queryAcctStatus.getKey());
		QueryAcctStatusResponse response = (QueryAcctStatusResponse) client.execute(request);
		if ("00000000".equals(response.getData())) {
			response.setActivateStatusEnum(ActivateStatusEnum.ACTIVATED);
		} else if ("00000001".equals(response.getData())) {
			response.setActivateStatusEnum(ActivateStatusEnum.NOTACTIVATE);
		} else {
			response.setActivateStatusEnum(ActivateStatusEnum.NOTACCT);
		}
		return response;
	}

	/**
	 * 虚账户单笔代发（支持虚转虚）-钱包转账功能、确认打款功能
	 * 
	 * @param request
	 * @return
	 */
	public VirAcctInResponse virAcctIn(VirAcctInRequest request) {
		request.setChannelUrl("C19VirSReTrigSer.API");
		request.setService(BoscServiceEnum.virAcctIn.getKey());
		return (VirAcctInResponse) client.execute(request);
	}

	/**
	 * 虚账号余额查询
	 * 
	 * @param request
	 * @return
	 */
	public QueryVirAcctBalanceResponse QueryVirAcctBalance(QueryVirAcctBalanceRequest request) {
		request.setChannelUrl("C19VirAcctBlanceInq.API");
		request.setService(BoscServiceEnum.queryVirAcctBalance.getKey());
		return (QueryVirAcctBalanceResponse) client.execute(request);
	}

	/**
	 * 虚账户提现
	 * 
	 * @param request
	 * @return
	 */
	public BoscResponseDomain withdraw(VirAcctWithdrawRequest request) {
		request.setChannelUrl("C19VirWithDrawalsInq.API");
		return client.execute(request);
	}

	/**
	 * 换绑功能
	 * 
	 * @param request
	 * @return
	 */
	public UpBindCardResponse upBindCard(UpBindCardRequest request) {
		request.setChannelUrl("C19UpdateBindCard.API");
		request.setService(BoscServiceEnum.upBindCard.getKey());
		return (UpBindCardResponse) client.execute(request);
	}

	/**
	 * 动态验证码发送（换绑功能）
	 * 
	 * @param request
	 * @return
	 */
	public BoscResponseDomain dynamicMobileCode(DynamicMobileCodeRequest request) {
		request.setChannelUrl("C19SdDyCodeInq.API");
		return client.execute(request);
	}

	/**
	 * 换绑激活
	 * 
	 * @param request
	 * @return
	 */
	public UpBindCardResponse activateUpBind(ActivateUpBindRequest request) {
		request.setChannelUrl("C19ActivationBindCard.API");
		request.setService(BoscServiceEnum.upBindCard.getKey());
		return (UpBindCardResponse) client.execute(request);
	}

}
