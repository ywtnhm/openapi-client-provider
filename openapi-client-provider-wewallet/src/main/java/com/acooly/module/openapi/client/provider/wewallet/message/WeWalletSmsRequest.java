package com.acooly.module.openapi.client.provider.wewallet.message;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wewallet.domain.WeWalletApiMsgInfo;
import com.acooly.module.openapi.client.provider.wewallet.domain.WeWalletRequest;
import com.acooly.module.openapi.client.provider.wewallet.enums.WeWalletServiceEnum;
import com.acooly.module.openapi.client.provider.wewallet.message.dto.WeWalletSmsInfo;

import lombok.Getter;
import lombok.Setter;

/**
 * @author fufeng
 */
@Getter
@Setter
@WeWalletApiMsgInfo(service = WeWalletServiceEnum.WEBANK_SMS, type = ApiMessageType.Request)
public class WeWalletSmsRequest extends WeWalletRequest {

    /**
     * 快捷申请业务信息
     */
    @ApiItem(value = "content")
    private WeWalletSmsInfo weWalletSmsInfo;
}
