package com.acooly.module.openapi.client.provider.wewallet.message.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * 签约订单参数
 * @author fufeng
 */
@Getter
@Setter
public class WeWalletQueryInfo implements Serializable {

    /**
     * 业务类型
     */
    protected String bizType;

    /**
     * 商户号
     */
    protected String merId;

    /**
     * 原交易订单号
     */
    protected String orderId;


    /**
     * 原交易类型
     */
    protected String origBizType;
}
