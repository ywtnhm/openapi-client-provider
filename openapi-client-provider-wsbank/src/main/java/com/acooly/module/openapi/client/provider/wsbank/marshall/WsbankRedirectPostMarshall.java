package com.acooly.module.openapi.client.provider.wsbank.marshall;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Service
@Slf4j
public class WsbankRedirectPostMarshall extends WsbankMarshallSupport implements ApiMarshal<PostRedirect, WsbankRequest> {

    @Override
    public PostRedirect marshal(WsbankRequest source) {
        PostRedirect postRedirect = new PostRedirect();
        if(Strings.isBlank(source.getGatewayUrl())) {
            source.setGatewayUrl(getProperties().getGatewayUrl());
        }
        postRedirect.setRedirectUrl(source.getGatewayUrl());
        log.info("跳转报文: {}", postRedirect);
        return postRedirect;
    }
}
