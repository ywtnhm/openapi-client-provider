package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsWechatChannel;
import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankResponseInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @author weili 2018/5/22 15:32
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankRegisterQueryResponseBody implements Serializable {

    /**
     * 返回码组件。当ResultStatus=S时才有后续的参数返回。
     */
    @XStreamAlias("RespInfo")
    private WsbankResponseInfo responseInfo;

    /**
     *商户号。网商为商户分配的商户号，通过商户入驻结果查询接口获取。
     */
    @XStreamAlias("MerchantId")
    private String merchantId;

    /**
     * 入驻结果。可选值：
     * 0：审核中
     * 1：成功
     * 2：失败
     */
    @XStreamAlias("RegisterStatus")
    private String registerStatus;

    /**
     *外部交易号
     */
    @XStreamAlias("OutTradeNo")
    private String outTradeNo;

    /**
     *网商支付订单号
     */
    @XStreamAlias("OrderNo")
    private String orderNo;

    /**
     *入驻失败原因返回
     */
    @XStreamAlias("FailReason")
    private String failReason;

    /**
     * 二类户卡号
     */
    @XStreamAlias("AccountNo")
    private String accountNo;


    /**
     * 支付宝线下进驻后的Smid
     */
    @XStreamAlias("Smid")
    private String smid;


    /**
     *线上支付宝进驻后的子商户号
     */
    @XStreamAlias("OnlineSmid")
    private String onlineSmid;


    /**
     * 标识进驻的微信渠道号、进驻结果、微信子商户号信息、进驻失败原因
     * ChannelId,Status(0处理中，1成功，2失败),WechatMerchId,FailReason
     */
    @XStreamAlias("WechatChannelList")
    private String  wechatChannelList;
    
    @XStreamOmitField
    private List<WsWechatChannel> wechatChannelListObj;

}
