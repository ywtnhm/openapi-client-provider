/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by ouwen
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.wsbank.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 借贷标识
 *
 * @author weili
 * Date: 2018-05-30 15:12:06
 */
public enum WsbankCreditTypeEnum implements Messageable {

    CREADIT("credit", "信用卡"),
    PCREDIT("pcredit","花呗（仅支付宝"),
    DEBIT("debit", "花呗（仅支付宝）"),
    BALANCE("balance","余额"),
    UNKNOWN("unknown","未知"),
    ;

    private final String code;
    private final String message;

    WsbankCreditTypeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (WsbankCreditTypeEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static WsbankCreditTypeEnum find(String code) {
        for (WsbankCreditTypeEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<WsbankCreditTypeEnum> getAll() {
        List<WsbankCreditTypeEnum> list = new ArrayList<WsbankCreditTypeEnum>();
        for (WsbankCreditTypeEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (WsbankCreditTypeEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
