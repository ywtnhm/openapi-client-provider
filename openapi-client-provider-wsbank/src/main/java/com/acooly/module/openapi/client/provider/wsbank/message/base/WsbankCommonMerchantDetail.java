package com.acooly.module.openapi.client.provider.wsbank.message.base;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author weili 2018/5/22 18:14
 */
@Getter
@Setter
public class WsbankCommonMerchantDetail implements Serializable {

	/**
	 * 商户简称。
	 */
	@JSONField(name="Alias")
	@Size(max = 20)
	@NotBlank
	private String alias;

	/**
	 * 联系人手机号。为商户常用联系人联系手机号。
	 */
	@Size(max = 20)
	@JSONField(name="ContactMobile")
	@NotBlank
	private String contactMobile;

	/**
	 * 联系人姓名。为商户常用联系人姓名。
	 */
	@Size(max = 256)
	@JSONField(name="ContactName")
	@NotBlank
	private String contactName;

	/**
	 * 省份
	 */
	@Size(max = 16)
	@JSONField(name="Province")
	@NotBlank
	private String province;

	/**
	 * 城市
	 */
	@Size(max = 16)
	@JSONField(name="City")
	@NotBlank
	private String city;

	/**
	 * 区
	 */
	@Size(max = 16)
	@JSONField(name="District")
	@NotBlank
	private String district;

	/**
	 * 地址
	 */
	@Size(max = 128)
	@JSONField(name="Address")
	@NotBlank
	private String address;

	/**
	 * 商户客服电话
	 */
	@NotBlank
	@Size(max = 32)
	@JSONField(name="ServicePhoneNo")
	private String servicePhoneNo;

	/**
	 * 邮箱
	 */
	@Size(max = 32)
	@JSONField(name="Email")
	private String email;

	/**
	 * 企业法人名称。
	 */
	@Size(max = 256)
	@JSONField(name="LegalPerson")
	private String legalPerson;

	/**
	 * 负责人手机号
	 */
	@NotBlank
	@Size(max = 32)
	@JSONField(name="PrincipalMobile")
	private String principalMobile;

	/**
	 * 负责人证件类型。可选值： 01：身份证
	 */
	@NotBlank
	@JSONField(name="PrincipalCertType")
	private String principalCertType = "01";

	/**
	 * 负责人证件号码。 备注：若自然人送自然人证件号码、若个体工商户送经营者证件号码、若企业送法人代表证件号码。
	 */
	@NotBlank
	@Size(max = 32)
	@JSONField(name="PrincipalCertNo")
	private String principalCertNo;

	/**
	 * 负责人名称或企业法人代表姓名。 备注：若商户类型为“自然人”填写责任人本人，若为“个体工商户”填写经营者本人，若为“企业”填写企业法人代表名称
	 */
	@NotBlank
	@Size(max = 256)
	@JSONField(name="PrincipalPerson")
	private String principalPerson;

	/**
	 * 营业执照工商注册号。若商户类型为“自然人”无需上送。
	 */
	@NotBlank
	@Size(max = 32)
	@JSONField(name="BussAuthNum")
	private String bussAuthNum;

	/**
	 * 组织机构代码证号。
	 */
	@Size(max = 32)
	@JSONField(name="CertOrgCode")
	private String certOrgCode;

	/**
	 * 负责人或企业法人代表的身份证图片正面
	 */
	@NotBlank
	@JSONField(name="CertPhotoA")
	private String certPhotoA;

	/**
	 * 负责人或企业法人代表的身份证图片反面
	 */
	@NotBlank
	@JSONField(name="CertPhotoB")
	private String certPhotoB;

	/**
	 * 营业执照图片
	 */
	@NotBlank
	@JSONField(name="LicensePhoto")
	private String licensePhoto;

	/**
	 * 组织机构代码证图片(若商户类型为“个体工商户”可选)
	 */
	@JSONField(name="PrgPhoto")
	private String prgPhoto;

	/**
	 * 开户许可证照片(若商户类型为“个体工商户”可选、企业必送)
	 */
	@JSONField(name="IndustryLicensePhoto")
	private String industryLicensePhoto;

	/**
	 * 门头照
	 */
	@JSONField(name="ShopPhoto")
	private String shopPhoto;

	/**
	 * 其他图片
	 */
	@JSONField(name="OtherPhoto")
	private String otherPhoto;

	/**
	 * 上送需关注的公众号对应的APPID
	 */
	@JSONField(name="SubscribeAppId")
	private String subscribeAppId;
	
	 @Override
	public String toString() {
	        return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}

}
