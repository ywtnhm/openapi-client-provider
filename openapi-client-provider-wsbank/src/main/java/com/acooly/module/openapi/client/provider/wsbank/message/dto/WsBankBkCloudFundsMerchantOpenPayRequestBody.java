package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * 开通商户余额支付能力
 * @author pangsw
 *
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsBankBkCloudFundsMerchantOpenPayRequestBody  implements Serializable  {

	private static final long serialVersionUID = -5812334189813986595L;
	
	/**
	 * 合作方机构号
	 */
	@Size(max = 32)
	@XStreamAlias("IsvOrgId")
	@NotBlank
	private String isvOrgId;
	
	/**
	 * 商户号
	 */
	@Size(max = 32)
	@XStreamAlias("MerchantId")
	@NotBlank
	private String merchantId;
	
	/**
	 * 外部交易号
	 */
	@Size(max = 64)
	@XStreamAlias("OutTradeNo")
	@NotBlank
	private String outTradeNo;	
	
	/**
	 * 请求时间
	 */
	@Size(max = 14)
	@XStreamAlias("RequestTime")
	@NotBlank
	private String requestTime;		
	

}
