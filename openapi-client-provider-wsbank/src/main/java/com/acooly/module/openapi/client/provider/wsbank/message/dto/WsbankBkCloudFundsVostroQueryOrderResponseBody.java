package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankResponseInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@XStreamAlias("body")
public class WsbankBkCloudFundsVostroQueryOrderResponseBody implements Serializable {

	private static final long serialVersionUID = 5680821849295136056L;

	/**
     * 返回码组件。当ResultStatus=S时才有后续的参数返回。
     */
    @XStreamAlias("RespInfo")
    private WsbankResponseInfo responseInfo;
    
	/**
	 * 原外部订单请求流水号
	 */
	@XStreamAlias("OutTradeNo")
	private String outTradeNo;
	
	/**
	 * 原网商订单号
	 */
	@XStreamAlias("OrderNo")
	private String orderNo;
	
	/**
	 * 入账流水号
	 */
	@XStreamAlias("TransNo")
	private String transNo;
	
	/**
	 * 出款渠道id（9001大额、9002小额、9003超网、9100支付宝）
	 */
	@XStreamAlias("ChannelId")
	private String channelId;
	
	/**
	 * 付款方银行联行号
	 */
	@XStreamAlias("PayerBankOrgId")
	private String payerBankOrgId;
	
	/**
	 * 付款方卡号
	 */
	@XStreamAlias("PayerCardNo")
	private String payerCardNo;	
	
	/**
	 * 付款方卡号户名
	 */
	@XStreamAlias("PayerCardName")
	private String payerCardName;	
	
	/**
	 * 订单金额(金额为分)
	 */
	@XStreamAlias("TotalAmount")
	private String totalAmount;	
	
	/**
	 * 币种，默认CNY
	 */
	@XStreamAlias("Currency")
	private String currency;
	
	/**
	 * 转账时间
	 */
	@XStreamAlias("TransferDate")
	private String transferDate;	
	
	/**
	 * 备注
	 */
	@XStreamAlias("Memo")
	private String memo;
	
	/**
	 * 扩展信息
	 */
	@XStreamAlias("ExtInfo")
	private String extInfo;
	
	/**
	 * 状态
	 */
	@XStreamAlias("Status")
	private String status;
}
