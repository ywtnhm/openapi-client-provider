package com.acooly.module.openapi.client.provider.yipay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayApiMsg;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayResponse;
import com.acooly.module.openapi.client.provider.yipay.enums.YipayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/4/18 11:38
 */
@Getter
@Setter
@YipayApiMsg(service = YipayServiceNameEnum.TWO_ELEM_VALID_BYNAME,type = ApiMessageType.Response)
public class YipayTwoElemValidByNameResponse extends YipayResponse {

    /**
     * 结果标识
     * Y-验证一致
     * N-验证不一致
     */
    private String stat;

    /**
     * 结果描述
     */
    private String desc;
}
