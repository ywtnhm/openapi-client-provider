package com.acooly.module.openapi.client.provider.yipay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayAlias;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayApiMsg;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayRequest;
import com.acooly.module.openapi.client.provider.yipay.enums.YipayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/4/18 11:38
 */
@Getter
@Setter
@YipayApiMsg(service = YipayServiceNameEnum.FOUR_ELEMENTS_VERIFY,type = ApiMessageType.Request)
public class YipayFourElementsVerifyRequest extends YipayRequest {

    /**
     * 银行账户号
     */
    @NotBlank
    @Size(min = 1,max = 30)
    @YipayAlias(value = "bankCardNo")
    private String bankCardNo;

    /**
     * 银行卡户名
     */
    @NotBlank
    @Size(max = 64)
    @YipayAlias(value = "bankCardName")
    private String realName;

    /**
     * 证件号码
     */
    @NotBlank
    @Size(max = 64)
    @YipayAlias(value = "certNo")
    private String certNo;

    /**
     * 预留手机号
     */
    @NotBlank
    @Size(min = 11,max = 11)
    @YipayAlias(value = "mobile")
    private String mobile;

    /**
     * 证件类型
     * 00 身份证
     */
    @NotBlank
    @Size(max = 2)
    @YipayAlias(value = "certType")
    private String certType = "00";
}
