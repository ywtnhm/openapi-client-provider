/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-17 17:49 创建
 */
package com.acooly.module.openapi.client.provider.yipay;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayApiMsg;

/**
 * @author zhangpu 2017-09-17 17:49
 */
public class YipayConstants {

    /**
     * 提供方
     */
    public static final String PROVIDER_NAME = "yipay";
    public static final String PROVIDER_DEF_PRINCIPAL = "principal";


    public static final String SUCCESS_RET_CODE = "0000";

    /**
     * 核心报文和协议字段
     */
    public static final String SERVICE_NAME = "serviceName";
    public static final String MERCHANT_NO = "merchantNo";
    public static final String ENCRYPT_KEY = "encryptKey";
    public static final String ENCRYPT_DATA = "encryptData";
    public static final String SIGN = "sign";

    public static final String RESPONSE_BODY_KEY = "result";

    public static final String REQUEST_PARAM_NAME = "notifyData";


    public static String getCanonicalUrl(String gatewayUrl, String serviceName) {
        String serviceUrl = gatewayUrl;
        serviceUrl = Strings.removeEnd(serviceUrl, "/");
        if (!Strings.startsWith(serviceName, "/")) {
            serviceUrl = serviceUrl + "/" + serviceName;
        } else {
            serviceUrl = serviceUrl + serviceName;
        }
        return serviceUrl;
    }

    public static String getServiceName(ApiMessage apiMessage) {
        if (Strings.isNotBlank(apiMessage.getService())) {
            return apiMessage.getService();
        }

        YipayApiMsg apiMsgInfo = getApiMsgInfo(apiMessage);
        if (apiMsgInfo != null && apiMsgInfo.service() != null) {
            return apiMsgInfo.service().code();
        }
        throw new RuntimeException("请求报文的service为空");
    }


    public static YipayApiMsg getApiMsgInfo(ApiMessage apiMessage) {
        return apiMessage.getClass().getAnnotation(YipayApiMsg.class);
    }

}
