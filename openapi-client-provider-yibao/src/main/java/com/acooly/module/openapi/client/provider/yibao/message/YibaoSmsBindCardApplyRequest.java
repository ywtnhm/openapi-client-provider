package com.acooly.module.openapi.client.provider.yibao.message;

import com.acooly.core.utils.Dates;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoAlias;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoApiMsg;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoRequest;
import com.acooly.module.openapi.client.provider.yibao.enums.YibaoServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author zhike 2018/6/26 10:16
 */
@Getter
@Setter
@YibaoApiMsg(service = YibaoServiceNameEnum.YIBAO_BINDCARD_REQUEST,type = ApiMessageType.Request)
public class YibaoSmsBindCardApplyRequest extends YibaoRequest {

    /**
     * 计费商编
     */
    @Size(max = 32)
    @YibaoAlias(value = "csmerchantno")
    private String csMerchantNo;

    /**
     * 用户标识
     * 商户生成的用户唯一标识
     */
    @Size(max = 32)
    @NotBlank
    @YibaoAlias(value = "identityid")
    private String identityId;

    /**
     * 用户标识类型
     * MAC：网卡地址
     * IMEI：国际移动设备标识
     * ID_CARD：用户身份证号
     * PHONE：手机号
     * EMAIL：邮箱
     * USER_ID：用户 id
     * AGREEMENT_NO：用户纸质订单协
     * 议号
     */
    @Size(max = 32)
    @NotBlank
    @YibaoAlias(value = "identitytype")
    private String identityType;

    /**
     * 卡号
     */
    @Size(max = 32)
    @NotBlank
    @YibaoAlias(value = "cardno")
    private String cardNo;

    /**
     * 证件号
     */
    @Size(max = 32)
    @NotBlank
    @YibaoAlias(value = "idcardno")
    private String idCardNo;

    /**
     * 证件类型
     */
    @Size(max = 32)
    @NotBlank
    @YibaoAlias(value = "idcardtype")
    private String idCardType = "ID";

    /**
     * 用户姓名
     * 只支持中文
     */
    @NotBlank
    @YibaoAlias(value = "username")
    private String realName;

    /**
     *手机号
     * 11 位数字
     */
    @NotBlank
    @YibaoAlias(value = "phone")
    private String mobileNo;

    /**
     * 建议短验发送类型
     * MESSAGE: 短验码将以短信的方式
     * 发送给用户
     * VOICE: 短验码将以语音的方式发送
     * 《新投资通——商户接入手册》 第 11 / 63页
     * 给用户
     * 默认值为 MESSAGE
     */
    @YibaoAlias(value = "advicesmstype")
    private String adviceSmsType;

    /**
     * 定制短验模板 ID
     * 用于需要定制短信验证码内容时，请联
     * 系您所属的技术支持或销售，此模板标
     * 识由易宝生成并告示商户
     */
    @YibaoAlias(value = "smstemplateid")
    private String smsTemplateId;

    /**
     * 补充鉴权类型
     * 暂不可用
     */
    @YibaoAlias(value = "customerenhancedtype")
    private String customerEnhancedType;

    /**
     * 绑卡订单有效期
     * 单位：分钟
     * 若不传则默认有效期 24h
     * 传的值要大于 1min，小于 48h
     * 传的值若小于 1min 系统置为 1min
     * 传的值若大于 48h 系统置为 48h
     */
    @YibaoAlias(value = "avaliabletime")
    private String avaliableTime;

    /**
     * 请求时间
     * 格式：yyyy-MM-dd HH:mm:ss
     */
    @NotBlank
    @YibaoAlias(value = "requesttime")
    private String requestTime = Dates.format(new Date(),Dates.CHINESE_DATETIME_FORMAT_LINE);
}
