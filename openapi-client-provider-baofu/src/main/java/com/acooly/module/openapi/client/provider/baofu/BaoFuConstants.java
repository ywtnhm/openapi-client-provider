/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-17 17:49 创建
 */
package com.acooly.module.openapi.client.provider.baofu;

/**
 * @author zhike 2017-09-17 17:49
 */
public class BaoFuConstants {
    /**
     * 签名实现KEY和秘钥
     */
    public static final String PROVIDER_NAME = "baofuProvider";
    public static final String BAOFU_DATA_CONTENT = "data_content";
    public static final String PARTNERID_KEY = "member_id";
    public static final String MD5_SIGNER_TYPE = "baoFuMd5";

    /**
     * 编码
     */
    public final static String ENCODE = "UTF-8";

    public final static String RSA_CHIPER = "RSA/ECB/PKCS1Padding";
    /**
     * 1024bit 加密块 大小
     */
    public final static int ENCRYPT_KEYSIZE = 117;
    /**
     * 1024bit 解密块 大小
     */
    public final static int DECRYPT_KEYSIZE = 128;

    /**
     * 宝付批量代扣(代收)和批量代扣(代收)查询求地址
     * 测试：https://vgw.baofoo.com/batchpay/api/batchTransRequest
     * 线上：https://public.baofoo.com/batchpay/api/batchTransRequest
     */
    public final static String BAOFU_BATCH_DEDUCT_URL="https://vgw.baofoo.com/batchpay/api/batchTransRequest";
    /**
     * 宝付单笔代扣(代收)和单笔代扣(代收)查询求地址
     * 测试：https://vgw.baofoo.com/cutpayment/api/backTransRequest
     * 线上：https://public.baofoo.com/cutpayment/api/backTransRequest
     */
    public final static String BAOFU_SINGLE_DEDUCT_URL="https://vgw.baofoo.com/cutpayment/api/backTransRequest";

    /**
     * 宝付提现(代付)接口请求地址
     * 测试：http://paytest.baofoo.com/baofoo-fopay/pay/BF0040001.do
     * 线上：https://public.baofoo.com/baofoo-fopay/pay/BF0040001.do
     */
    public final static String BAOFU_WITHDRAW_URL="http://paytest.baofoo.com/baofoo-fopay/pay/BF0040001.do";

    /**
     * 宝付提现(代付)查询接口请求地址
     * 测试：http://paytest.baofoo.com/baofoo-fopay/pay/BF0040002.do
     * 线上：https://public.baofoo.com/baofoo-fopay/pay/BF0040002.do
     */
    public final static String BAOFU_WITHDRAW_QUERY_URL="http://paytest.baofoo.com/baofoo-fopay/pay/BF0040002.do";

    /**
     * 宝付对账文件下载接口请求地址
     * 测试：https://vgw.baofoo.com/boas/api/fileLoadNewRequest
     * 线上：https://public.baofoo.com/boas/api/fileLoadNewRequest
     */
    public final static String BAOFU_DOWNLOAD_BILL_URL="https://vgw.baofoo.com/boas/api/fileLoadNewRequest";

    /**
     * 宝付退款请求地址
     * 测试：https://vgw.baofoo.com/cutpayment/api/backTransRequest
     * 线上：https://public.baofoo.com/cutpayment/api/backTransRequest
     */
    public final static String BAOFU_REFUND="https://vgw.baofoo.com/cutpayment/api/backTransRequest";

}
