/*
 * 修订记录:
 * zhike@yiji.com 2017-08-24 17:06 创建
 *
 */
package com.acooly.module.openapi.client.provider.baofu.message;

import com.acooly.core.common.exception.OrderCheckException;
import com.acooly.core.common.facade.ResultCode;
import com.acooly.core.utils.validate.Validators;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuRequest;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.acooly.module.openapi.client.provider.baofu.message.info.BaoFuWithdrawRequestInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 修订记录：
 *
 * @author zhike@yiji.com
 */
@Getter
@Setter
@XStreamAlias("trans_content")
@BaoFuApiMsgInfo(service = BaoFuServiceEnum.WITHDRAW,type = ApiMessageType.Request)
public class BaoFuWithdrawRequest extends BaoFuRequest{

    @XStreamAlias("trans_reqDatas")
    private List<BaoFuWithdrawRequestInfo> withdrawInfos;

    @Override
    public void doCheck() {
        super.doCheck();
        if(withdrawInfos == null) {
            throw new OrderCheckException(ResultCode.PARAMETER_ERROR.getCode(),"批量提交提现申请不能为空");
        }else if(withdrawInfos.size() > 5) {
            throw new OrderCheckException(ResultCode.PARAMETER_ERROR.getCode(),"批量提交提现申请数量不能一次超过5条");
        }
        for(BaoFuWithdrawRequestInfo withdrawInfo:withdrawInfos) {
            Validators.assertJSR303(withdrawInfo);
        }
    }
}
