/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by ouwen
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.baofu.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * inst_channel_api DebitCreditType 枚举定义
 *
 * @author zhike Date: 2017-03-30 15:12:06
 */
public enum BaoFuServiceEnum implements Messageable {
  DEDUCT("deduct", "13", "代扣"),
  TRADE_QUERY("tradeQuery", "31", "订单查询"),
  BATCH_DEDUCT("batchDeduct", "32", "批量代扣"),
  BATCH_DEDUCT_SIGLE_QUERY("batchDeductSigleQuery", "20", "批量代扣单笔查询"),
  BATCH_DEDUCT_BATCH_QUERY("batchDeductBatchQuery", "19", "批量代扣批量查询"),
  WITHDRAW("withdraw", "BF0040001", "提现"),
  WITHDRAW_QUERY("withdrawQuery", "BF0040002", "提现订单查询"),
  BILL_DOWNLOAD("billDowdload", "0", "下载对账文件"),
  REFUND_QUERY("refundQuery", "10", "退款查询"),
  REFUND("refund", "09", "退款"),
  ACCOUNT_BALANCE_QUERY("accountBalanceQuery", "BF0001", "余额查询"),
  ;

  private final String code;
  private final String key;
  private final String message;

  private BaoFuServiceEnum(String code, String key, String message) {
    this.code = code;
    this.message = message;
    this.key = key;
  }

  public String getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }

  public String code() {
    return code;
  }

  public String message() {
    return message;
  }

  public String getKey() {
    return key;
  }

  public static Map<String, String> mapping() {
    Map<String, String> map = new LinkedHashMap<String, String>();
    for (BaoFuServiceEnum type : values()) {
      map.put(type.getCode(), type.getMessage());
    }
    return map;
  }

  /**
   * 通过枚举值码查找枚举值。
   *
   * @param code 查找枚举值的枚举值码。
   * @return 枚举值码对应的枚举值。
   * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
   */
  public static BaoFuServiceEnum find(String code) {
    for (BaoFuServiceEnum status : values()) {
      if (status.getCode().equals(code)) {
        return status;
      }
    }
    return null;
  }

  /**
   * 通过枚举值码查找枚举值。
   *
   * @param key 查找枚举值的枚举值码。
   * @return 枚举值码对应的枚举值。
   * @throws IllegalArgumentException 如果 key 没有对应的 Status 。
   */
  public static BaoFuServiceEnum findByKey(String key) {
    for (BaoFuServiceEnum status : values()) {
      if (status.getKey().equals(key)) {
        return status;
      }
    }
    return null;
  }

  /**
   * 获取全部枚举值。
   *
   * @return 全部枚举值。
   */
  public static List<BaoFuServiceEnum> getAll() {
    List<BaoFuServiceEnum> list = new ArrayList<BaoFuServiceEnum>();
    for (BaoFuServiceEnum status : values()) {
      list.add(status);
    }
    return list;
  }

  /**
   * 获取全部枚举值码。
   *
   * @return 全部枚举值码。
   */
  public static List<String> getAllCode() {
    List<String> list = new ArrayList<String>();
    for (BaoFuServiceEnum status : values()) {
      list.add(status.code());
    }
    return list;
  }
}
