package com.acooly.module.openapi.client.provider.baofu.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuNotify;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.acooly.module.openapi.client.provider.baofu.support.BaoFuAlias;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/1/26 13:56
 */
@Getter
@Setter
@BaoFuApiMsgInfo(service = BaoFuServiceEnum.DEDUCT,type = ApiMessageType.Notify)
@XStreamAlias("result")
public class BaoFuDeductNotify extends BaoFuNotify{

    /**
     * 成功金额 单位：分
     */
    @XStreamAlias("succ_amt")
    private String succAmt;
}
