/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月5日
 *
 */
package com.acooly.module.openapi.client.provider.fuiou.message;

import com.acooly.core.utils.Collections3;
import com.acooly.module.openapi.client.provider.fuiou.domain.FuiouResponse;
import com.acooly.module.openapi.client.provider.fuiou.message.dto.BalanceResult;
import com.google.common.collect.Lists;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.util.List;

/**
 * 余额查询 响应报文
 * 
 * @author zhangpu
 */
public class BalanceActionResponse extends FuiouResponse {

	@XStreamAlias("results")
	public List<BalanceResult> results = Lists.newArrayList();

	public List<BalanceResult> getResults() {
		return results;
	}

	public void setResults(List<BalanceResult> results) {
		this.results = results;
	}

	public String getUserId() {
		return Collections3.getFirst(this.results).getUserId();
	}

	public String getCtBalance() {
		return Collections3.getFirst(this.results).getCtBalance();
	}

	public String getCaBalance() {
		return Collections3.getFirst(this.results).getCaBalance();
	}

	public String getCfBalance() {
		return Collections3.getFirst(this.results).getCfBalance();
	}

	public String getCuBalance() {
		return Collections3.getFirst(this.results).getCuBalance();
	}

}
