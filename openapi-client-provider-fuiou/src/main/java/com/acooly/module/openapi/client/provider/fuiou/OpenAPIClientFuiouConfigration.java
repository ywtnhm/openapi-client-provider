package com.acooly.module.openapi.client.provider.fuiou;

import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import static com.acooly.module.openapi.client.provider.fuiou.OpenAPIClientFuiouProperties.PREFIX;

@EnableConfigurationProperties({OpenAPIClientFuiouProperties.class})
@ConditionalOnProperty(value = PREFIX + ".enable", matchIfMissing = true)
@ComponentScan
public class OpenAPIClientFuiouConfigration {

    @Autowired
    private OpenAPIClientFuiouProperties openAPIClientFuiouProperties;

    @Bean("fuiouHttpTransport")
    public Transport fuiouHttpTransport() {
        HttpTransport httpTransport = new HttpTransport();
        httpTransport.setGateway(openAPIClientFuiouProperties.getGateway());
        httpTransport.setConnTimeout(String.valueOf(openAPIClientFuiouProperties.getConnTimeout()));
        httpTransport.setReadTimeout(String.valueOf(openAPIClientFuiouProperties.getReadTimeout()));
        return httpTransport;
    }

}
